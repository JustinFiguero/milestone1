using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Tankdata))]
public class Health : MonoBehaviour, IAttackable
{
    private Tankdata m_data;
    private float m_currenetHealth = 100f;


    private void Start()
    {
        m_data = gameObject.GetComponent<Tankdata>();

        if (m_data != null)
        {
            m_currenetHealth = m_data.maxHealth;
        }
    }

    public float CurrentHealth
    {
        get
        {
            return m_currenetHealth;
        }
        set
        {
            m_currenetHealth = value;
            if (m_currenetHealth <= 0f)
            {
                //Die if health hits 0
                Die();
            }
            if(m_currenetHealth > m_data.maxHealth)
            {
                m_currenetHealth = m_data.maxHealth;
            }
        }
    }

    public void OnAttacked(Attack attackData)
    {
        CurrentHealth -= attackData.m_damage;
    }

    public void Die()
    {
        //do the death stuff.
        Debug.Log("Tank was killed");
        Destroy(this.gameObject);
    }
}
