using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    public float m_cannonballLifespan = 2f;
    public Attack m_attackData = new Attack();

    // Update is called once per frame
    void Update()
    {
        m_cannonballLifespan -= Time.deltaTime;
        if (m_cannonballLifespan <=0)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        // destroy cannonball at all costs
        IAttackable[] attackables = collision.gameObject.GetComponents<IAttackable>();
        if (attackables == null)
        {
            return;
        }
        //apply damage
        foreach (IAttackable attackable in attackables)
        {
            attackable.OnAttacked(m_attackData);
        }
        Destroy(this.gameObject);
    }
}
