using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Tankdata))]
[RequireComponent(typeof(Tankmotor))]
[RequireComponent(typeof(Tankshooter))]
public class PlayerController : MonoBehaviour
{
    private Tankdata m_data;
    private Tankmotor m_motor;
    private Tankshooter m_shooter;

    public enum InputScheme { WASD, ArrowKeys };
    public InputScheme m_playerInputScheme = InputScheme.WASD;

    private void Start()
    {
        m_data = gameObject.GetComponent<Tankdata>();
        m_motor = gameObject.GetComponent<Tankmotor>();
        m_shooter = gameObject.GetComponent<Tankshooter>();
    }

    // Update is called once per frame
    private void Update()
    {
       switch (m_playerInputScheme)
       { 
            case InputScheme.ArrowKeys:
        break;
            case InputScheme.WASD:
                //handle movement
                if (Input.GetKey(KeyCode.W))
                {
                    m_motor.Move(m_data.moveSpeed);
                }
                else if (Input.GetKey(KeyCode.S))
                {
                    m_motor.Move(-m_data.moveSpeed);
                }
                else // we need to pass in a movement speed of 0 to ensure gravity is being used.
                {
                    m_motor.Move(0f);
                }
                //handle rotation
                if (Input.GetKey(KeyCode.A))
                {
                    m_motor.Rotate(-m_data.rotateSpeed);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    m_motor.Rotate(m_data.rotateSpeed);
                }
                //handle shooting
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    m_shooter.Shoot();
                }
                break;
            default:
                Debug.LogWarning("[PlayerController] unimplemented Input Scheme used.");
                break;
    }
    
        
           
        }
}
