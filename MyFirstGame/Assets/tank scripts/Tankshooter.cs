using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Tankdata))]
public class Tankshooter : MonoBehaviour

   
{
    private Tankdata m_data;
    public GameObject m_cannonBallPrefab;
    public float m_cooldownTimer = 0f;
    public GameObject m_firePoint;
    //TODO: cooldown timer on shooting.
    private void Start()
    {
        m_data = gameObject.GetComponent<Tankdata>();

    }

    private void Update()
    {
        if (!CanShoot())
        {
            m_cooldownTimer -= Time.deltaTime;
        }
    }
    public void Shoot()
    {
        if (CanShoot())
        {
            Debug.Log("Pew Pew");

            GameObject cannonBall = Instantiate(m_cannonBallPrefab, m_firePoint.transform.position, Quaternion.identity);
            //TODO: add force to the cannon ball.
            cannonBall.GetComponent<Rigidbody>().AddForce(transform.forward * m_data.cannonBallForce, ForceMode.Impulse);
            //get attack
            //get refernce to cannoballl class
            CannonBall m_cannonBallData = cannonBall.GetComponent<CannonBall>();
            // set attacker
            m_cannonBallData.m_attackData.m_attacker = this.gameObject;
            // set damage
            m_cannonBallData.m_attackData.m_damage = m_data.damagePerShot;
            //reset the cooldown timer
            m_cooldownTimer = m_data.secondsPerShot;
        }
        
    }
    public bool CanShoot()
    {
        return (m_cooldownTimer <= 0f);
}
}

