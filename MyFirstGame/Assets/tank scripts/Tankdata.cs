using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tankdata : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float rotateSpeed = 30f;
    public float secondsPerShot = 1f;
    public float cannonBallForce = 1f;
    public float damagePerShot = 1f;
    public float maxHealth = 100f;
}
